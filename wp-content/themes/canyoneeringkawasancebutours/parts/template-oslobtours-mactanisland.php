
<style type="text/css">

</style>

<div class="clearfix" style="margin:5px 0px">
<div class="col-sm-5">
<h4 class="title_parts">MACTAN ISLAND HOPPING DAY TOUR</h4>
<?php include(__DIR__  . '/carousel_mactan.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 5 -->

<div class="col-sm-7">



<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
            <table class="table table-condensed">
            <tr><td style="width:20%"><strong>No. of Person(s)</strong></td>
            <td style="width:40%">Price per Head (Hopping at Nalusuan, Hilutungan and Caohagan islands)</td>
            </tr>
            <tr><td>1</td><td>6,100/head</td></tr>
            <tr><td>2</td><td>4,150/head </td></tr>
            <tr><td>3</td><td>3,500/head</td></tr>
            <tr><td>4</td><td>3,300/head</td></tr>
            <tr><td>5</td><td>3,200/head </td></tr>
            <tr><td>6</td><td>3,100/head</td></tr>
            <tr><td>7</td><td>3,000/head</td></tr>
            <tr><td>8</td><td>2,900/head </td></tr>
            <tr><td>9</td><td>2,800/head</td></tr>
            <tr><td>10</td><td>2,700/head</td></tr>
            <tr><td>11</td><td>2,600/head</td></tr>
            <tr><td>12</td><td>2,500/head</td></tr>
            <tr><td>13</td><td>2,400/head</td></tr>
            <tr><td>14 and Above</td><td>Contact us for the price quotation</td>
            </tr>

            <tr><td colspan="2">
            <strong>NOTE:</strong><br/>HILUTUNGAN Island ( We only dock near the island for fish feeding, swimming &amp; snorkelling)<br/><br/>
              <strong>Children / Kids Pricing</strong>
              <ul>
              <li>Child below 2 years old - FREE/Not Included in Head Count</li>
              <li>Child 3 to 5 years old - Less 20%</li>
              <li>Child above 5 years old - Full Charge</li>
              </ul>
            </td></tr>
            </table>

                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          Entrance fees<br>
          Lunch<br>
          Life Jacket <br>
          Fully air-conditioned Car or Van Service <br>
          Boat service (Motorized Banca/Pump Boat)<br>
          </td></tr>
          </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2">
          Breakfast and Dinnter<br>
          Snacks<br>
          Underwater Camera<br>
          Accommodations<br/>
          Air Fare<br/>
          </td></tr>
          </table>
                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2">Recommended pick up time from Metro Cebu, Mandaue and Lapu-lapu City between 6:00AM to 9:00AM</td></tr>
          <tr><td style="width:35%">8:00 AM</td><td style="width:65%">Pick up and departure time from Hotel </td></tr>
          <tr><td>9:00 AM</td><td> Arrival at Mactan and departure time </td></tr>
          <tr><td>9:00 AM </td><td> Tour at islands, swimming and snorkeling  </td></tr>
          <tr><td>1200 NN to 1:30 PM </td><td> Lunch and cool off  </td></tr>
          <tr><td>2:00PM </td><td> back to Mactan  </td></tr>
          <tr><td>2:30 PM </td><td> depart back to Hotel  </td></tr>
          <tr><td>3:30 PM </td><td> Arrival at hotel </td></tr>
          <tr><td colspan="2">Island Tour duration is maximum for 10 hrs. In case you want to extend/side trip (ex: Cebu City Tour), we charge P 300.00/hr.</td></tr>
          </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-hand-right"></span> DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">1 Day private tour (8-10 hours duration)</td></tr>
          <tr><td colspan="2">Boatman as guide</td></tr>
          <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
           <tr><td colspan="2">
           <ul>
           <li>1 to 3 persons &#45; sedan type vehicle</li>
           <li>4 to 6 persons &#45; minivan type vehicle</li>
           <li>7 to 13 persons &#45; van type vehicle</li>
           <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
           </ul>
           </td></tr>
          </table>

                </div>
            </div>
          </div>

</div> <!-- accordion -->

</div> <!-- col 6 -->
</div>



<script type="text/javascript">

  var package_tour_name = "MACTAN ISLAND HOPPING";
  var pkprice_per_head = [6100,4150,3500,3300,3200,3100,3000,2900,2800,2700,2600,2500,2400];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

    setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>