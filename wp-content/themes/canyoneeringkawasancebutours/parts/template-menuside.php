  <form id="form_compute">
  <input type="hidden" name="submitted" value="true">
  <input type="hidden" name="tour_name" id="tour_name" value="">

  <table class="table table-condensed table_compute" id="table_compute" cellpadding="none" cellspacing="none">

  <tr><td colspan="2" style="background-color:#222222; color:white; padding:10px">PLEASE FILL-UP REQUIRED DETAILS</td></tr>

  <tr><td>
  Full Name
  </td><td>
  <input type="text" name="full_name" class="form-control" id="full_name" placeholder="Enter Full Name" required>
  </td></tr>
  <tr><td>
  Email
  </td><td>
  <input type="email" name="email" class="form-control" id="email" placeholder="Enter Best Email" required>
  </td></tr>
  <tr><td>
  Contact
  </td><td>
  <input type="text" name="contact" class="form-control" id="contact" placeholder="Enter Contact Number" required>
  </td></tr>
  <tr><td>
  Pick-up Date
  </td><td>
  <input type="text" name="pick_date" class="form-control" id="pick_date" placeholder="mm/dd/yyyy">
  </td></tr>
  <tr><td>
  Pick-up Location
  </td><td>
  <input type="text" name="pick_location" class="form-control" id="pick_location" placeholder="Hotel/Airport">
  </td></tr>

  <tr><td><span class="impt_text">No. of Person(s)</td>
  <td>
    <select name="no_of_persons" class="form-control" id="no_of_persons" style="width:100%">
    </select>
  </td>
  </tr>

        <tr><td>
        RATE/HEAD:
        </td><td>
        <input type="text" class="form-control" name="price_head_adult" id="price_head_adult" readonly>
        </td></tr>

        <tr><td>
        TOTAL RATE:
        </td><td>
        <input type="text" class="form-control" name="computed_total_price" id="computed_total_price" readonly>
        </td></tr>

  <tr><td colspan=2>
  Additional Info<br/>
  <textarea class="noresize form-control" name="other_info" id="other_info"  rows="3" placeholder="Additional Info"></textarea>
  </td></tr>

        <tr><td colspan="2" style="text-align:center">
        <input  type="submit" value="RESERVE NOW" id="reserve_14" style="background-color:#0b9600;height:60px;font-size:1.3em;border:0px;width:100%"><br/>
        </td></tr>

  </table>
  </form>



