  <div id="homeCarousel" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image001.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image002.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image020.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image003.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image004.jpg" alt="Canyoneering Cebu">
      </div>
    </div>
  </div>