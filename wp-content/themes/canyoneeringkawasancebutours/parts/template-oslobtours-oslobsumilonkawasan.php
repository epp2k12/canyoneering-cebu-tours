
<style type="text/css">

</style>

<div class="clearfix" style="margin:5px 0px">
<div class="col-sm-5">
<h4 class="title_parts">KAWASAN CANYONEERING, OSLOB WHALE &amp; SUMILON ISLAND</h4>
<?php include(__DIR__  . '/carousel_osk.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 5 -->

<div class="col-sm-7">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
            <table class="table table-condensed">
            <tr><td style="width:30%">No. of Person(s)</td><td style="width:70%">Price per Head(s) in PHP</td></tr>
            <tr><td>1</td><td>9,700/head</td></tr>
            <tr><td>2</td><td>6,500/head </td></tr>
            <tr><td>3</td><td>5,500/head</td></tr>
            <tr><td>4</td><td>4,800/head</td></tr>
            <tr><td>5</td><td>4,200/head </td></tr>
            <tr><td>6</td><td>3,900/head</td></tr>
            <tr><td>7</td><td>3,800/head</td></tr>
            <tr><td>8</td><td>3,700/head </td></tr>
            <tr><td>9</td><td>3,600/head</td></tr>
            <tr><td>10</td><td>3,500/head</td></tr>
            <tr><td>11</td><td>3,400/head</td></tr>
            <tr><td>12</td><td>3,300/head</td></tr>
            <tr><td>13</td><td>3,200/head</td></tr>
            <tr><td>14 and Above</td><td>Contact us for the price quotation</td>
            </tr>
            <tr><td colspan="3">
              <strong>Important notes:</strong>
              <ul>
              <li>*** For foreign guest, it has an add-on fee of P500/head in whale shark and not included in this package.</li>
              <li>*** Children below 12yo are not allowed at Canyoneering. Children with parents or guardians will go directly to Kawasan Falls</li>
              <li>*** Side Trip is subject to surcharge</li>
              </ul>

            </td></tr>
            </table>
                  </div>
              </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          <ul>
          <li>Whale watching</li>
          <li>Boat ride and life vest During Whale Watching</li>
          <li>Sumilon Sandbar Fee</li>
                <li>Pumpboat service roundtrip sumilon</li>
          <li>Short Trekking Canyoneering </li>
          <li>Local Guide, life vest, safety helmet, aqua shoes</li>
              <li>Fully air-conditioned Car or Van Service</li>
          </ul>
          </td></tr>
          </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          <ul>
          <li>Meals (Breakfast, Lunch, Dinner)</li>
          <li>Airfare</li>
          <li>Hotel Accommodations<li/>
          <li>Snorkeling gear in Sumilon<li/>
                <li>Bluewater resort</li>
          </ul>
          </td></tr>
          </table>

                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2">Recommended pick up time from Cebu City or Lapu-lapu City is between 3:15AM</td></tr>
          <tr><td style="width:35%">03:15 AM</td><td style="width:65%">Pick up and departure time from Hotel </td></tr>
          <tr><td>05:45 AM</td><td>Arrival in Oslob and Breakfast (Guest Expense)</td></tr>
          <tr><td>06:15 AM</td><td>Whale shark watching and snorkeling</td></tr>
          <tr><td>07:30 AM</td><td>Depart to Sumilon</td></tr>
          <tr><td>10:00 AM</td><td>Depart for Kawasan</td></tr>
          <tr><td>11:30 AM</td><td>Arrival at Kawasan Falls and Lunch (Guest Expense)</td></tr>
          <tr><td>12:00 NN</td><td>Start Short Trekking Canyoneering</td></tr>
          <tr><td>03:00 PM</td><td>Cool Off at Kawasan Falls</td></tr>
                <tr><td>04:30 PM</td><td>Depart for Cebu City</td></tr>
          <tr><td>07:30 PM</td><td>Arrival in Cebu City/Hotel</td></tr>
          </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-hand-right"></span> DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">1 Day private tour</td></tr>
          <tr><td colspan="2">Tour facilitator</td></tr>
          <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
           <tr><td colspan="2">
           <ul>
           <li>1 to 3 persons &#45; sedan type vehicle</li>
           <li>4 to 6 persons &#45; minivan type vehicle</li>
           <li>7 to 13 persons &#45; van type vehicle</li>
           <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
           </ul>
           </td></tr>
          </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-hand-right"></span> CHECKLIST FOR CANYONEERING</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2">
            Must be 12 years old and above<br>
            Knows how to swim <br>
            Bring extra shorts and shirts<br>
            Sandals or boots<br>
            Extra bottled water<br>
            Travel insurance<br>
          </td></tr>
          </table>
                </div>
            </div>
          </div>


</div> <!-- accordion -->
</div> <!-- col 7 -->

<script type="text/javascript">
  var package_tour_name = "OSLOB WHALE / SUMILON ISLAND / CANYONEERING";
  var pkprice_per_head = [9700,6500,5500,4800,4200,3900,3800,3700,3600,3500,3400,3300,3200];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

    setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>