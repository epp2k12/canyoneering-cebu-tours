
<style type="text/css">

</style>

<div class="clearfix" style="margin:5px 0px">
<div class="col-sm-5">
<h4 class="title_parts">CEBU SOUTH OSLOB OVERNIGHT BUDGET TOUR</h4>
<?php include(__DIR__  . '/carousel_overbudget.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 5 -->

<div class="col-sm-7">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
            <table class="table table-condensed">
            <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
            <tr><td>1</td><td>13,000 / head</td></tr>
            <tr><td>2</td><td>7,900 / head</td></tr>
            <tr><td>3</td><td>6,500 / head</td></tr>
            <tr><td>4</td><td>5,800 / head</td></tr>
            <tr><td>5</td><td>5,000 / head</td></tr>
            <tr><td>6</td><td>4,600 / head</td></tr>
            <tr><td>7</td><td>4,500 / head</td></tr>
            <tr><td>8</td><td>4,300 / head</td></tr>
            <tr><td>9</td><td>4,100 / head</td></tr>
            <tr><td>10</td><td>3,900 / head</td></tr>
            <tr><td>11</td><td>3,600 / head</td></tr>
            <tr><td>12</td><td>3,300 / head</td></tr>
            <tr><td>13 and Above</td><td>Contact us for the price quotation</td></tr>
                  <tr>
            <tr><td colspan="3">
              <strong>Important notes:</strong>
              <ul>
              <li>*** For foreign guest, it has an add-on fee of P500/head in whale shark and not included in this package.</li>
              <li>*** Side Trip is subject to surcharge</li>
              </ul>
            </td></tr>
            </table>

                  </div>
              </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          <ul>
          <li>Whale Watching Fee</li>
          <li>Boat ride for whale watching</li>
          <li>Life vest and snorkeling gear during whale watching</li>
          <li>Fully Air-conditioned car or van service</li>
          <li>Room Accommodation 1Night at Lagnasonâ€™s Place resort</li>
          <li>Boat transfer service to Sumilon and back</li>
          <li>Tumalog Falls fee and motorbike service</li>
          <li>Osmena Peak fee</li>
          </ul>
          </td></tr>
          </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          <ul>
          <li>Airfare</li>
          <li>Underwater Camera (You can rent from the locals)</li>
          <li>Meals (Breakfast, Lunch &amp; Dinner)</li>
          <li>Snorkeling gear in sumilon</li>
          <li>Bluewater resort in sumilon</li>
          </ul>
          </td></tr>
          </table>

                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
        <table class="table table-condensed">
          <tr><td colspan="2" style="background-color:#faffaf">DAY 1</td></tr>
          <tr><td style="width:35%">07:00 AM</td><td>Pickup at any point in Metro Cebu (Hotels/Airport)</td></tr>
          <tr><td>09:30 AM</td><td>Arrival at Osmena Peak</td></tr>
          <tr><td>11:00 AM</td><td>Depart for Oslob</td></tr>
          <tr><td>12:00 NN</td><td>Arrival at Oslob Resort / Check-in / Lunch</td></tr>
          <tr><td>01:00 PM</td><td>Depart for Tumalog Falls</td></tr>
          <tr><td>04:30 PM</td><td>Depart back to Resort</td></tr>
          <tr><td>06:00 PM</td><td>Dinner at Resort</td></tr>
          <tr><td>07:00 PM</td><td>Relax or Night Swimming at Resort</td></tr>
              <tr><td colspan="2"  style="background-color:#faffaf">DAY 2</td></tr>
              <tr><td style="width:35%">06:00 AM</td><td>Breakfast </td></tr>
              <tr><td>06:30 AM</td><td>Depart to Oslob Whale Watching</td></tr>
              <tr><td>08:00 AM</td><td>Depart to Sumilon Island</td></tr>
              <tr><td>11:00 AM</td><td>Depart to Oslob</td></tr>
              <tr><td>11:30 AM</td><td>Check out resort</td></tr>
              <tr><td>12:00 NN</td><td>Lunch Time</td></tr>
              <tr><td>01:00 PM</td><td>Depart to Simala Shrine</td></tr>
              <tr><td>02:30 PM</td><td>Arrival Simala Shrine</td></tr>
              <tr><td>04:00 PM</td><td>Depart to Cebu City</td></tr>
              <tr><td>06:30 PM</td><td>Arrival in Cebu City</td></tr>
            </table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->
</div> <!-- col 6 -->

<script type="text/javascript">
  var package_tour_name = "OSLOB OVERNIGHT (2D/1N) BUDGET PACKAGE";
  var pkprice_per_head = [13000,7900,6500,5800,5000,4600,4500,4300,4100,3900,3600,3300];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

    setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>