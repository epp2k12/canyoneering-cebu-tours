
<style type="text/css">

</style>

<div class="clearfix" style="margin:5px 0px">
<div class="col-sm-5">
<h4 class="title_parts">CEBU CITY DAY TOUR</h4>
<?php include(__DIR__  . '/carousel_cebu.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 5 -->

<div class="col-sm-7">

<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
            <table class="table table-condensed">
            <tr><td style="width:30%">No. of Person(s)</td><td style="width:70%">Price per Head ()</td></tr>
            <tr><td>1</td><td>2,900/head</td></tr>
            <tr><td>2</td><td>1,600/head </td></tr>
            <tr><td>3</td><td>1,200/head</td></tr>
            <tr><td>4</td><td>1,050/head</td></tr>
            <tr><td>5</td><td>900/head </td></tr>
            <tr><td>6</td><td>800/head</td></tr>
            <tr><td>7</td><td>750/head</td></tr>
            <tr><td>8</td><td>700/head </td></tr>
            <tr><td>9</td><td>650/head</td></tr>
            <tr><td>10</td><td>600/head</td></tr>
            <tr><td>11</td><td>550/head</td></tr>
            <tr><td>12</td><td>500/head</td></tr>
            <tr><td>13</td><td>500/head</td></tr>
            <tr><td>14 and Above</td><td>Contact us for the price quotation</td>
            </tr>
            <tr><td colspan="3">
              Children / Kids Pricing
              <ul>
              <li>Child below 2 years old - FREE/Not included in headcount</li>
              <li>Child 3 to 7 years old - Less Php300</li>
              <li>Child above 8 years old - Full Charge</li>
              </ul>
            </td></tr>
            </table>

                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          Entrance fees<br>
          Fully air-conditioned Car or Van Service<br>
          </td></tr>
          </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2">
            MEALS/SNACKS<br>
            Accommodations<br/>
          Air Fare<br/>
          </td></tr>
          </table>
                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td width="30%">9:00 AM </td><td width="70%"> Pick up and departure time from Hotel </td></tr>
          <tr><td>9:45 AM &#45; 11:30 AM </td><td> Fort San Pedro, Magellan&#39;s Cross, Santo Nino, Cebu Heritage Monument, Yap-Sandiego Ancestral House  </td></tr>
          <tr><td>1200 NN &#45; 1:30 PM </td><td> Lunch Break</td></tr>
          <tr><td>1:30 PM &#45; 4:00 PM </td><td> Museo Sugbo, Cebu Taoist Temple, Mactan Shrine </td></tr>
          <tr><td>5:00 PM </td><td>Back at hotel  </td></tr>
          <tr><td colspan="2">Tour duration is maximum for 8 hrs. In case you want to have side trip, we will charge P 300.00/hr.</td></tr>
          </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-hand-right"></span> DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">1 Day private tour (8 hours duration)</td></tr>
          <tr><td colspan="2">Driver as guide</td></tr>
          <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
           <tr><td colspan="2">
           <ul>
           <li>1 to 3 persons &#45; sedan type vehicle</li>
           <li>4 to 6 persons &#45; minivan type vehicle</li>
           <li>7 to 13 persons &#45; van type vehicle</li>
           <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
           </ul>
           </td></tr>
          </table>

                </div>
            </div>
          </div>



</div> <!-- accordion -->

</div> <!-- col 6 -->
</div>



<script type="text/javascript">

  var package_tour_name = "CEBU CITY DAY TOUR";
  var pkprice_per_head = [2900,1600,1200,1050,900,800,750,700,650,600,550,500,500];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

    setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>