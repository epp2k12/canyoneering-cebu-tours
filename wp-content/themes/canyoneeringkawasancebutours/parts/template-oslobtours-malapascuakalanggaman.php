
<style type="text/css">

</style>

<div class="clearfix" style="margin:5px 0px">
<div class="col-sm-5">
<h4 class="title_parts">MALAPASCUA &amp; KALANGGAMAN ISLAND OVERNIGHT</h4>
<?php include(__DIR__  . '/carousel_malagaman.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 5 -->

<div class="col-sm-7">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
            <table class="table table-condensed">
            <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>

            <tr><td>2</td><td>14,800 / head</td></tr>
            <tr><td>3</td><td>11,200 / head</td></tr>
            <tr><td>4</td><td>9,200 / head</td></tr>
            <tr><td>5</td><td>8,400 / head</td></tr>
            <tr><td>6</td><td>7,800 / head</td></tr>
            <tr><td>7</td><td>6,900 / head</td></tr>
            <tr><td>8</td><td>6,600 / head</td></tr>
            <tr><td>9</td><td>6,400 / head</td></tr>
            <tr><td>10</td><td>6,300 / head</td></tr>
            <tr><td>11</td><td>6,200 / head</td></tr>
            <tr><td>12</td><td>6,100 / head</td></tr>
            <tr><td>13</td><td>6,000 / head</td></tr>
            <tr><td>14 and above</td><td>Contact us for the price quotation</td></tr>
          </table>

                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          <ul>

          <li>Private Fully Air-conditioned car or van service â€“ Roundtrip transfer from City-Maya Port</li>
          <li>Room Accommodation 1Night at Malapascua resort (Fan room type)</li>
          <li>Roundtrip Pumpboat Service to/from Maya port to Malapascua/Kalanggaman</li>
          <li>Life Jacket</li>
          <li>Meals: 2 Breakfasts, 2 Lunches, 1 Dinner</li>
          <li>Tour Guide Fee</li>
          <li>Islands Entrance fee</li>
          <li>Private charter boat for island tours</li>
          <li>Free videoke (for 9heads and above)</li>


          </ul>
          </td></tr>
          </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2">
          <ul>

          <li>Airfare</li>
          <li>Underwater Camera</li>
          <li>Snorkeling gear</li>


          </ul>
          </td></tr>
          </table>
                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2" style="background-color:#faffaf">DAY 1</td></tr>

          <tr><td style="width:35%">04:00am</td><td>Pickup at Hotel/Airport</td></tr>
          <tr><td>07:00am</td><td>   Arrival at Maya Port</td></tr>
          <tr><td>07:15am</td><td>   Depart to Malapascua island</td></tr>
          <tr><td>08:00am</td><td>   Arrival at Malapascua resort</td></tr>
          <tr><td>08:30am</td><td>   Breakfast</td></tr>
          <tr><td>09:00am</td><td>   Depart to Kalanggaman island</td></tr>
          <tr><td>11:00am</td><td>   Arrival Kalanggaman island, swimming/snorkeling</td></tr>
          <tr><td>12:00nn</td><td>   Lunch</td></tr>
          <tr><td>03:00pm</td><td>   Depart back to Malapascua</td></tr>
          <tr><td>05:00pm</td><td>   Arrival at Malapascua resort</td></tr>
          <tr><td>06:30pm</td><td>   Dinner

          <tr><td colspan="2"  style="background-color:#faffaf">DAY 2</td></tr>

          <tr><td style="width:35%">07:30am</td><td>Breakfast</td></tr>
          <tr><td>08:30am</td><td>    Malapascua Tour</td></tr>
          <tr><td>11:00am</td><td>   Back at resort</td></tr>
          <tr><td>12:00nn</td><td>   Lunch Time</td></tr>
          <tr><td>01:00pm</td><td>   Depart to Maya Port</td></tr>
          <tr><td>02:00pm</td><td>   Arrival Maya Port</td></tr>
          <tr><td>02:15pm</td><td>   Depart to Cebu City</td></tr>
          <tr><td>05:30pm</td><td>   Arrival in Cebu City</td></tr>

          </table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->
</div> <!-- col 7 -->

<script type="text/javascript">

  var package_tour_name = "OVERNIGHT MALAPASCUA KALANGGAMAN ISLAND";
  var pkprice_per_head = [14800,11200,9200,8400,7800,6900,6600,6400,6300,6200,6100,6000];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

    setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ (i+1) + '">' + (i+1) + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons-1,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons-1,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>

