<style type="text/css">
* {
    box-sizing: border-box;
}
p {
	text-align: justify;
}
.textpart {
	padding:6px;
	
}
</style>
<div>
<img src="<?php bloginfo('stylesheet_directory')?>/images/main_product/modular_cabinets/modular_cabinet01cropped.jpg" />

<div class="textpart" style="">
<p>
Cabinets can take up a big chunk of your Bedroom and Kitchen Renovation Works and Budget. Customizing cabinets comes with huge labor costs. Also when done incorrectly may take up a large amount of your space and may look a clutter and less efficient. We at ABM, we design/customize cabinets to make it functional, beautiful, easy to install yet just right for your budget.
</p>
<p>
We manufacture the best modular cabinets for your Bedrooms, living rooms and kitchen. One big advantage on this approach is that each is designed to meet your individual needs, it can make your kitchen unique or just plain simple. We can combine upper, lower and full cabinets, grouped it to fit the size and style of your kitchen and we can do variety of finishes and colors to give your kitchen your personal touch. 
</p>
</div>

<?php
		$args=array(
		'cat' => 6,
		'order' => DESC
		);
		$packagePosts = new WP_Query( $args );
		if( $packagePosts->have_posts() ) {
			while ( $packagePosts->have_posts() )  {
					 $packagePosts->the_post();    
				echo "<h1 style='border:1px solid #a15b00;text-align:center;color:#a15b00;'>VIEW SOME OF OUR PROJECTS HERE</div>";
				the_content();
			}
		}
		
?>

</div>