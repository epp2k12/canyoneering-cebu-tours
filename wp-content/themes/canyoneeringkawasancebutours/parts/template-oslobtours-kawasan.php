
<style type="text/css">

</style>

<div class="clearfix" style="margin:5px 0px">
<div class="col-sm-5">
<h4 class="title_parts">KAWASAN FALLS &amp; CANYONEERING</h4>
<?php include(__DIR__  . '/carousel_kawasan.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 5 -->

<div class="col-sm-7">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                  <div class="panel-body tour_details">

            <table class="table table-condensed">
            <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
            <tr><td>1</td><td>6,000/head</td></tr>
            <tr><td>2</td><td>3,700/head </td></tr>
            <tr><td>3</td><td>3,100/head</td></tr>
            <tr><td>4</td><td>2,800/head</td></tr>
            <tr><td>5</td><td>2,600/head </td></tr>
            <tr><td>6</td><td>2,400/head</td></tr>
            <tr><td>7</td><td>2,300/head</td></tr>
            <tr><td>8</td><td>2,100/head </td></tr>
            <tr><td>9</td><td>2,000/head</td></tr>
            <tr><td>10</td><td>1,900/head</td></tr>
            <tr><td>11</td><td>1,800/head</td></tr>
            <tr><td>12</td><td>1,750/head</td></tr>
            <tr><td>13</td><td>1,700/head</td></tr>
            <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
            <tr><td colspan="2">
            <p>

            <strong>Add-on Fee:</strong><br/>
            <ul>
            <li>If you want to use bamboo raft = P600</li>
            <li>if you want to use bamboo shed = P300</li>
            <li>if you want to use bamboo cottage = P1,800</li>
            </ul>
            </p>
            </td></tr>
            </table>

                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          Entrance fees<br>
          Life Jacket<br>
          Motorbike ride to jump off point<br>
          Waterproof bag and shoes<br>
          Safety helmet<br>
          Fully air-conditioned Car or Van Service<br>
          </td></tr>
          </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          Underwater Camera<br>
            MEALS/SNACKS<br>
            Accommodations<br/>
          Air Fare<br/>
          </td></tr>
          </table>

                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2">Recommended pick up time from Cebu City or Lapu-lapu City between 4:00AM to 6:00AM</td></tr>
          <tr><td style="width:35%">5:00 AM</td><td style="width:65%">Pick up and departure time from Hotel </td></tr>
          <tr><td>7:30 AM</td><td>Arrival at Badian then Breakfast </td></tr>
          <tr><td>8:15 AM</td><td>Motorbike to jump off point</td></tr>
          <tr><td>12:00 NN</td><td>End of canyoneering at Kawasan falls</td></tr>
          <tr><td>1200 NN &#45; 1:30 PM</td><td>Lunch and cool off </td></tr>
          <tr><td>1:45 PM</td><td>Depart from Badian</td></tr>
          <tr><td>4:30 PM</td><td>Arrival in Metro Cebu</td></tr>
          <tr><td colspan="2">Tour duration is maximum for 10 hrs. In case you want to have side trip (upon arrival in Cebu City), we charge P 300.00/hr.</td></tr>
          </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">1 Day private tour (10 hours duration)</td></tr>
          <tr><td colspan="2">Tour facilitator</td></tr>
          <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
           <tr><td colspan="2">
           <ul>
           <li>1 to 3 persons &#45; sedan type vehicle</li>
           <li>4 to 6 persons &#45; minivan type vehicle</li>
           <li>7 to 13 persons &#45; van type vehicle</li>
           <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
           </ul>
           </td></tr>
          </table>

                </div>
            </div>
          </div>


</div> <!-- accordion -->
</div> <!-- col 7 -->
<div class="clearfix package_caption">
<h3> CANYONEERING CEBU DAY TOUR</h3>
<p>
Experience a whole adventure of canyoneering kawasan falls cebu day tour in Badian and Alegria, South of Cebu Philippines. The activity involves climbing uphill slopes, river trekking, jumping off the waterfalls, swimming the natural fresh water pools, and sometimes requires rappelling. The downstream canyoneering activity will start from Kanlaob River in Alegrea and ends at Kawasan Falls in Badian. Canyoneering cebu day tour takes 3 to 5 hours depending on your speed and number of people in a group. A larger group would need more time. Ulitmately the activity will finish while you relax and swim in the fresh waters of the majestic Kawasan Falls. This canyoneering kawasan falls activity has been the favorite for many adventure loving tourists both foreigners and locals alike.
</p>
<p>
Very Affordable Canyoneering Kawasan Falls that you can avail now at Canyoneering Cebu Tours. The rate is already very low for such a great outdoor activity for you and your family.
</p>
</div>

  <script type="text/javascript">
  var package_tour_name = "KAWASAN FALLS / CANYONEERING";
  var pkprice_per_head = [6000,3700,3100,2800,2600,2400,2300,2100,2000,1900,1800,1750,1700];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

    setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>
