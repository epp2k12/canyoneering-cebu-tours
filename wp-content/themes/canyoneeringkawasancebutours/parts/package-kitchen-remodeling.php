<style type="text/css">
* {
    box-sizing: border-box;
}
p {
	text-align: justify;
}
.textpart {
	padding:6px;
	
}

/* ------------- styling the image slider tour_Gallery ----------------- */

    #swap_images img{
        display:none;
        position:absolute;
        top:0;
        left:0;
        padding:4px;
        margin:2px;
        border-radius: 11px;
        -moz-box-shadow: 3px 3px 4px #444;
        -webkit-box-shadow: 3px 3px 4px #444;
        box-shadow: 3px 3px 4px #444;
      }

    #swap_images img.active{
        display:block;
      }
   
   .image_swap {
        position:relative;
        min-height:280px;
        text-align:auto;
   }

</style>
<div>
<img src="<?php bloginfo('stylesheet_directory')?>/images/main_product/kitchen_remodelling/kitchen_remodeling.jpg" />

<div class="textpart" style="">
<p>
No room is quite as multifunctional as the kitchen. The hub of the home, this space has evolved from a strictly utilitarian unit into a versatile room to prepare food, entertain guests and share meals. If you’re looking to do a kitchen remodel, keep in mind that a successful kitchen design needs to blend functionality with personal prerequisites
</p>
<p>
When discovering kitchen ideas, there are several aspects to consider and keep in mind as you browse kitchen photos. First and foremost, you should carefully consider your layout and where to place large appliances. Next, you should focus on storage; kitchens contain a lot of utensils, pots, pans and gadgets, and you need to have enough space to store all of your favorites within easy reach. Lastly, your room should reflect your personality with its decor and vibe. The ultimate goal of all kitchens should be to create the most functional yet beautiful space possible to meet your eating and entertaining needs. 
</p>
</div>

<?php
		$args=array(
		'cat' => 8,
		'order' => DESC
		);
		$packagePosts = new WP_Query( $args );
		if( $packagePosts->have_posts() ) {
			while ( $packagePosts->have_posts() )  {
					 $packagePosts->the_post();    
				echo "<h1 style='border:1px solid #a15b00;text-align:center;color:#a15b00;'>VIEW SOME OF OUR PROJECTS HERE</div>";
				the_content();
			}
		}
		
?>

</div>