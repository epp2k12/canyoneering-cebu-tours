
<style type="text/css">

</style>

<div class="clearfix" style="margin:5px 0px">
<div class="col-sm-5">
<h4 class="title_parts">PESCADOR ISLAND HOPPING &amp; CANYONEERING DAY TOUR</h4>
<?php include(__DIR__  . '/carousel_pescan.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 5 -->

<div class="col-sm-7">



<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
            <table class="table table-condensed">
            <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
            <tr><td>1</td><td>9,500/head</td></tr>
            <tr><td>2</td><td>6,800/head </td></tr>
            <tr><td>3</td><td>5,200/head</td></tr>
            <tr><td>4</td><td>4,800/head</td></tr>
            <tr><td>5</td><td>4,400/head </td></tr>
            <tr><td>6</td><td>4,200/head</td></tr>
            <tr><td>7</td><td>4,000/head</td></tr>
            <tr><td>8</td><td>3,800/head </td></tr>
            <tr><td>9</td><td>3,400/head</td></tr>
            <tr><td>10</td><td>3,200/head</td></tr>
            <tr><td>11</td><td>3,000/head</td></tr>
            <tr><td>12</td><td>2,800/head</td></tr>
            <tr><td>13</td><td>2,600/head</td></tr>
            <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
            </table>

                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
                <ul>
                <li> Private Fully Air-conditioned car or van service </li>
                <li> Pescador Island Hopping </li>
                  <ul>
                    <li>Roundtrip Pumpboat Service</li>
                    <li>Life Jacket</li>
                    <li>Snorkeling gear</li>
                    <li>Boatman as guide</li>
                  </ul>
                <li>Canyoneering</li>
                  <ul>
                    <li>Entrance Fee</li>
                    <li>Canyoneering Tour Guide</li>
                    <li>Life vest</li>
                    <li>Safety Helmet</li>
                    <li>Aqua Shoes</li>
                  </ul>
                </ul>
          </td></tr>
          </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2">

          <ul>
          <li>Airfare</li>
          <li>Underwater Camera</li>
          <li>Accomodation</li>
          <li>Meals</li>
          <li>Bamboo shed/cottage at Kawasan</li>
          <li>Bamboo raft at Kawasan</li>
          </ul>
          </td></tr>
          </table>
                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td style="width:35%">04:00am </td><td>   Pickup at Hotel/Airport</td></tr>
          <tr><td>06:30am </td><td>  Arrival at Moalboal and breakfast </td></tr>
          <tr><td>07:00am </td><td>  Start Pescador Island Hopping</td></tr>
          <tr><td>11:00am </td><td>  Back to Moalboal</td></tr>
          <tr><td>11:30am </td><td>  Lunch</td></tr>
          <tr><td>01:00nn </td><td>  Start Canyoneering</td></tr>
          <tr><td>04:00pm </td><td>  Cool off at Kawasan</td></tr>
          <tr><td>05:00pm </td><td>  Depart to Cebu City</td></tr>
          <tr><td>08:00pm </td><td>  Arrival Cebu City</td></tr>
          </table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->

</div> <!-- col 6 -->
</div>



<script type="text/javascript">

  var package_tour_name = "PESCADOR ISLAND HOPPING & KAWASAN CANYONEERING DAY TOUR";
  var pkprice_per_head = [9500,6800,5200,4800,4400,4200,4000,3800,3400,3200,3000,2800,2600];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

    setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>