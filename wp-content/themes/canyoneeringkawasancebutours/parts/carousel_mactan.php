  <div id="homeCarousel" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image021.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image022.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image023.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image024.jpg" alt="Canyoneering Cebu">
      </div>
    </div>
  </div>