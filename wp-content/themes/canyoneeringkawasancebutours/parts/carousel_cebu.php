  <div id="homeCarousel" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image008.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image014.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image016.jpg" alt="Canyoneering Cebu">
      </div>
      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/images/parts_carousel/image017.jpg" alt="Canyoneering Cebu">
      </div>

    </div>
  </div>