<style type="text/css">
* {
    box-sizing: border-box;
}
p {
  text-align: justify;
}
.textpart {
  padding:6px;
  
}
</style>
<div>
<img src="<?php bloginfo('stylesheet_directory')?>/images/main_product/furniture_fitouts/furniture_fitouts.jpg" />

<div class="textpart" style="">
<p>
We design furnitures that captures your personal style with quality craftsmanship. From traditional classics to modern and unique styling. We also make accent furnitures and accessories ( fit-outs  ) to complete your home needs. They can add beauty, elegance and theme to your room. We can turn your empty space to something out of the ordinary. We use the best materials and the best texture results.
</p>
</div>

<?php
    $args=array(
    'cat' => 7,
    'order' => DESC
    );
    $packagePosts = new WP_Query( $args );
    if( $packagePosts->have_posts() ) {
      while ( $packagePosts->have_posts() )  {
           $packagePosts->the_post();    
        echo "<h1 style='border:1px solid #a15b00;text-align:center;color:#a15b00;'>VIEW SOME OF OUR PROJECTS HERE</div>";
        the_content();
      }
    }
    
?>

</div>
