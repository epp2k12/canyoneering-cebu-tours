
<style type="text/css">

</style>

<div class="clearfix" style="margin:5px 0px">
<div class="col-sm-5">
<h4 class="title_parts">OSLOB WHALE, TUMALOG FALLS &amp; SIMALA SHRINE</h4>
<?php include(__DIR__  . '/carousel_ots.php'); ?>
<?php include(__DIR__  . '/template-menuside.php'); ?>
</div> <!-- col 5 -->

<div class="col-sm-7">
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PRICING</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                  <div class="panel-body tour_details">
            <table class="table table-condensed">
            <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
            <tr><td>1</td><td>5,450head</td></tr>
            <tr><td>2</td><td>3,350/head </td></tr>
            <tr><td>3</td><td>2,800/head</td></tr>
            <tr><td>4</td><td>2,400/head</td></tr>
            <tr><td>5</td><td>2,200/head </td></tr>
            <tr><td>6</td><td>1,950/head</td></tr>
            <tr><td>7</td><td>1,850/head</td></tr>
            <tr><td>8</td><td>1,800/head </td></tr>
            <tr><td>9</td><td>1,750/head</td></tr>
            <tr><td>10</td><td>1,700/head</td></tr>
            <tr><td>11</td><td>1,650/head</td></tr>
            <tr><td>12</td><td>1,600/head</td></tr>
            <tr><td>13</td><td>1,550/head</td></tr>
            <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
            <tr>
            <tr><td colspan="3">
              <strong>Important notes:</strong>
              <ul>
              <li>*** For foreign guest, it has an add-on fee of P500/head in whale shark and not included in this package.</li>
              <li>*** Side Trip is subject to surcharge</li>
              </ul>
            </td></tr>
            </table>

                  </div>
              </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          Whale watching and Tumalog falls entrance<br>
          Boat ride and life vest<br>
          Whale shark watching<br>
          Tumalog Falls<br>
          Fully air-conditioned Car or Van Service<br>
          </td></tr>
          </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">
          MEALS/Snacks<br>
          Underwater Camera<br>
          Air Fare<br/>
          Accomodations<br/>
          </td></tr>
          </table>

                </div>
            </div>
          </div>


          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-hand-right"></span> CLICK TO VIEW ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
          <table class="table table-condensed">
          <tr><td colspan="2">Recommended pick up time from Cebu City or Lapu-lapu City is between 4:00AM to 5:30AM</td></tr>
          <tr><td style="width:35%">05:00 AM </td><td style="width:65%"> Pick up and departure time from Hotel </td></tr>
          <tr><td>08:00 AM </td><td> Arrival at Oslob then Breakfast </td></tr>
          <tr><td>08:30 AM </td><td> Whale Shark Watching </td></tr>
          <tr><td>09:30 AM </td><td> Tumalog Falls </td></tr>
          <tr><td>11:30 AM </td><td> Lunch </td></tr>
          <tr><td>12:30 PM </td><td> Depart from Oslob for Simala</td></tr>
          <tr><td>02:30 PM </td><td> Arrival Simala Shrine</td></tr>
          <tr><td>03:30 PM </td><td> Depart to Cebu City</td></tr>
          <tr><td>06:00 PM </td><td> Arrival Cebu City </td></tr>
          <tr><td colspan="2">In case you want to have side trip (upon arrival in Cebu City), we charge P300.00/hr. </td></tr>
          </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><span class="glyphicon glyphicon-hand-right"></span> DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">

          <table class="table table-condensed">
          <tr><td colspan="2">1 Day private tour (15 hours duration)</td></tr>
          <tr><td colspan="2">Tour Facilitator</td></tr>
          <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
           <tr><td colspan="2">
           <ul>
           <li>1 to 3 persons &#45; sedan type vehicle</li>
           <li>4 to 6 persons &#45; minivan type vehicle</li>
           <li>7 to 13 persons &#45; van type vehicle</li>
           <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
           </ul>
           </td></tr>
          </table>

                </div>
            </div>
          </div>

</div> <!-- accordion -->
</div> <!-- col 6 -->

<script type="text/javascript">
  var package_tour_name = "OSLOB WHALE / TUMALOG FALLS / SIMALA";
  var pkprice_per_head = [5450,3350,2800,2400,2200,1950,1850,1800,1750,1700,1650,1600,1550];
  var num_persons =0;
  var d_price_head =0;
  var d_total_rate =0;
  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  jQuery(document).ready( function(){

    setInterval('swapImages("swap_images")', 5000);

    //initialize tour name
    jQuery('#tour_name').val(package_tour_name);
    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    //initialize the rate per head and total rate
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    jQuery('#no_of_persons').change(function(){
        num_persons = jQuery('select[name=no_of_persons]').val();
        computeTotalRate(num_persons,pkprice_per_head);
    });
    //handles submit button functions
    submitCompute();
  });

</script>
