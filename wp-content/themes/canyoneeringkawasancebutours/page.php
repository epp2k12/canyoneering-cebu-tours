<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package abm
 */

get_header(); ?>

<style type="text/css">
* {
    box-sizing: border-box;
}
</style>

<?php if(is_front_page()) : ?>

<div id="myCarousel" class="carousel slide" data-interval="5000" data-ride="carousel">
  <!-- Indicators 
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>
  -->

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">

    <div class="item active">
         <img src="<?php bloginfo('stylesheet_directory')?>/images/main_carousel/main02.jpg" alt="Kawasan Canyoneering"/>
      <div class="main-car-caption">
      <h4>Book with us and experience the thrills of Canyoning in Kawasan Falls.</h4>
      <p>With us the adventure is safe and unforgettable ... </p>
      </div>
    </div>

    <div class="item">
          <img src="<?php bloginfo('stylesheet_directory')?>/images/main_carousel/main04.jpg" alt="Kawasan Canyoneering"/>
      <div class="main-car-caption">
      <h4>Enjoy the activity with your family and friends.</h4>
      <p>Thrill seekers and nature trippers are welcome ...</p>
      </div>
    </div>

    <div class="item">
          <img src="<?php bloginfo('stylesheet_directory')?>/images/main_carousel/main05.jpg" alt="Kawasan Canyoneering"/>
      <div class="main-car-caption">
      <h4>Experience rock climbing, cliff jumping and more</h4>
      <p>We provide you with safety gears.</p>
      </div>
    </div>

    <div class="item">
          <img src="<?php bloginfo('stylesheet_directory')?>/images/main_carousel/main07.jpg" alt="Kawasan Canyoneering"/>
      <div class="main-car-caption">
      <h4>We will guide you through this amazing journey</h4>
      <p>We have the most able and friendly tour guides</p>
      </div>
    </div>

    <div class="item">
          <img src="<?php bloginfo('stylesheet_directory')?>/images/main_carousel/main08.jpg" alt="Kawasan Canyoneering"/>
      <div class="main-car-caption">
      <h4>To eternity and beyond.</h4>
      <p>The Ultimate YOLO experience ...</p>
      </div>
    </div>

    <div class="item">
          <img src="<?php bloginfo('stylesheet_directory')?>/images/main_carousel/main09.jpg" alt="Kawasan Canyoneering"/>
      <div class="main-car-caption">
      <h4>If they can do it, so can you</h4>
      <p>Gear up, and complete the ultimate challenge ... </p>
      </div>
    </div>

    <div class="item">
          <img src="<?php bloginfo('stylesheet_directory')?>/images/main_carousel/main10.jpg" alt="Kawasan Canyoneering"/>
      <div class="main-car-caption">
      <h4>We will guide you all the way</h4>
      <p>We have highly trained canyoneering tour guides</p>
      </div>
    </div>

    <div class="item">
      <img src="<?php bloginfo('stylesheet_directory')?>/images/main_carousel/main12.jpg" alt="Kawasan Canyoneering"/>
      <div class="main-car-caption">
      <h4>The only impossible journey is the one you never begin</h4>
      <p>We offer the most affordable package for a great canyoneering experience</p>
      </div>
    </div>

    <div class="item">
          <img src="<?php bloginfo('stylesheet_directory')?>/images/main_carousel/main13.jpg" alt="Kawasan Canyoneering"/>
      <div class="main-car-caption">
      <h4>Difficult roads often leads to beautiful destinations</h4>
      <p>Enjoy the magnificent view of the great Kawasan falls ... </p>
      </div>
    </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

<div class="inside_the_front_page" style="overflow:auto">

	<div class="kawasan_main clearfix row">

		<div class="col-sm-5" style="">
		<img src="<?php bloginfo('stylesheet_directory')?>/images/tours/tour01.jpg" alt="Kawasan Canyoneering"/>
		</div>

		<div class="col-sm-7" style="">
		<h4 style="margin:0px;padding;0px"><span class="glyphicon glyphicon-ok-sign"></span> KAWASAN FALLS CANYONEERING</h4>
		<ul>
		<li>Check-in for Canyoneering in Badian</li>
		<li>Briefing and Local Tour Guide Assignment</li>
		<li>Trail begins in Kanlaob River, Alegria</li>
		<li>Exciting Rock Climbing, Cliff Jumping and River Swimming</li>
		<li>Trail end, Relax and enjoy swimming in Kawasan Falls</li>
		</ul>

		 <div class="buttons_for_kawasan">
		 <a href="http://localhost/webPage02/kawasan-falls-canyoneering-day-tour/" class="btn btn-success btn-lg button_kawasan">Book this tour now!</a>
		 </div>

		</div>

	</div>

	<div class="kawasan_main clearfix row">

		<div class="col-sm-5" style="">
		<img src="<?php bloginfo('stylesheet_directory')?>/images/tours/tour02.jpg" alt="Kawasan Canyoneering"/>
		</div>

		<div class="col-sm-7" style="">
		<h4 style="margin:0px;padding;0px"><span class="glyphicon glyphicon-ok-sign"></span> KAWASAN FALLS CANYONEERING * TUMALOG FALLS * OSLOB WHALE SHARK SNORKELING</h4>
		<ul>
		<li>Briefing and Local Tour Guide Assignment</li>
		<li>Trail begins in Kanlaob River, Alegria</li>
		<li>Exciting Rock Climbing, Cliff Jumping and River Swimming</li>
		<li>Tumalog Falls Watching</li>
		<li>Oslob Whale Watching/Swimming and Snorkeling</li>
		</ul>

		 <div class="buttons_for_kawasan">
		 <a href="http://localhost/webPage02/kawasan-canyoneering-oslob-whale-shark-and-tumalog-falls/" class="btn btn-success btn-lg button_kawasan">Book this tour now!</a>
		 </div>

		</div>

	</div>

	<div class="kawasan_main clearfix row">

		<div class="col-sm-5" style="">
		<img src="<?php bloginfo('stylesheet_directory')?>/images/tours/tour03.jpg" alt="Kawasan Canyoneering"/>
		</div>

		<div class="col-sm-7" style="">
		<h4 style="margin:0px;padding;0px"><span class="glyphicon glyphicon-ok-sign"></span> KAWASAN FALLS CANYONEERING * SUMILON ISLAND SANDBAR * OSLOB WHALE SHARK SNORKELING</h4>
		<ul>
		<li>Check-in for Canyoneering in Badian</li>
		<li>Briefing and Local Tour Guide Assignment</li>
		<li>Trail begins in Kanlaob River, Alegria</li>
		<li>Exciting Rock Climbing, Cliff Jumping and River Swimming</li>
		<li>Enjoy the beautiful Sandbar in Sumilon Island</li>
		<li>Oslob Whale Watching/Swimming and Snorkeling</li>
		</ul>

		 <div class="buttons_for_kawasan">
		 <a href="http://localhost/webPage02/kawasan-canyoneering-oslob-whale-shark-and-sumilon-island/" class="btn btn-success btn-lg button_kawasan">Book this tour now!</a>
		 </div>

		</div>

	</div>

	<div class="kawasan_main clearfix row">

		<div class="col-sm-5" style="">
		<img src="<?php bloginfo('stylesheet_directory')?>/images/tours/tour04.jpg" alt="Kawasan Canyoneering"/>
		</div>

		<div class="col-sm-7" style="">
		<h4 style="margin:0px;padding;0px"><span class="glyphicon glyphicon-ok-sign"></span> KAWASAN FALLS CANYONEERING * PESCADOR ISLAND HOPPING</h4>
		<ul>
		<li>Check-in for Canyoneering in Badian</li>
		<li>Briefing and Local Tour Guide Assignment</li>
		<li>Trail begins in Kanlaob River, Alegria</li>
		<li>Exciting Rock Climbing, Cliff Jumping and River Swimming</li>
        <LI>Explore the beautiful Island of Pescador</li>
		</ul>

		 <div class="buttons_for_kawasan">
		 <a href="http://localhost/webPage02/pescador-island-and-kawasan-canyoneering-day-tour/" class="btn btn-success btn-lg button_kawasan">Book this tour now!</a>
		 </div>

		</div>

	</div>

</div> <!--  class="inside_the_front_page" -->



<?php else: ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php endif; ?>

<div class="clearfix"></div>


<script type="text/javascript">
// alert("hello world!");
$(document).ready(function(){
   // alert("inside jquery!");

$('.main_product').hover(function() {
		$(this).find('.main_product_description').slideToggle();
});

$('.home_contact h1').hover(function() {
	//$(this).css("color","000000");
});

$('[data-toggle="tooltip"]').tooltip();   

});
</script>


<?php

// get_sidebar();
get_footer();
?>
