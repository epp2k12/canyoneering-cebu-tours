<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link http://canyoneeringcebutours.com
 *
 * @package canyoneeringcebutours
 */
?>
<div class="front_group">
<h3 class="pages_title_bars">FAVORITE DAY TOUR PACKAGES</h3>
<?php
$args=array(
  'cat' => 12,
  'order' => ASC
);
?>
  <?php
  $packagePosts = new WP_Query( $args );
  if( $packagePosts->have_posts() ) {
  //loop through related posts based on the tag
    while ( $packagePosts->have_posts() ) :
    $packagePosts->the_post();
  ?>

  <div class="tour_package col-sm-3 col-lg-2 col-xs-6">
  <a href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail(); ?>

    <p class="dtitle">
    <?php the_title(); ?>
    </p>

  </a>
  <div class="book_now">
  <a href="<?php the_permalink(); ?>" style="color:#cccccc"><strong>BOOK NOW!</strong></a>
  </div>

  </div>

  <?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php } ?>
</div>

<div class="front_group">
<h3 class="pages_title_bars">FAVORITE OVERNIGHT AND MULTIDAY TOUR PACKAGES</h3>
<?php
$args=array(
  'cat' => 13,
  'order' => ASC
);
?>
  <?php
  $packagePosts = new WP_Query( $args );
  if( $packagePosts->have_posts() ) {
  //loop through related posts based on the tag
    while ( $packagePosts->have_posts() ) :
    $packagePosts->the_post();
  ?>

  <div class="tour_package col-sm-3 col-lg-2 col-xs-6">
  <a href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail(); ?>

    <p class="dtitle">
    <?php the_title(); ?>
    </p>

  </a>
  <div class="book_now">
  <a href="<?php the_permalink(); ?>" style="color:#cccccc"><strong>BOOK NOW!</strong></a>
  </div>

  </div>

  <?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php } ?>
</div>

<div class="home_contact">
	<div class="contact_label"><h1>
	<span class="glyphicon glyphicon-envelope"></span>
	CUSTOMIZE YOUR TOUR!</h1>
	</div>

<div class="col-xs-12 col-sm-5">

<!--
<h1 data-toggle="tooltip" title="Just send us a mail"><span class="glyphicon glyphicon-ok-sign"></span> WE TALK</h1>
<h1 data-toggle="tooltip" title="We listen to your needs and ideas!"><span class="glyphicon glyphicon-ok-sign"></span> WE LISTEN</h1>
<h1 data-toggle="tooltip" title="Our experts will give you the best designs that suits your need!"><span class="glyphicon glyphicon-ok-sign"></span> WE SUGGEST</h1>
<h1><span class="glyphicon glyphicon-ok-sign"></span> WE DESIGN</h1>
<h1><span class="glyphicon glyphicon-ok-sign"></span> WE BUILD</h1>
<h1><span class="glyphicon glyphicon-ok-sign"></span> WE FINISH</h1>
<h1><span class="glyphicon glyphicon-ok-sign"></span> WE SUPPORT</h1>
-->

<form id="form_easybooking">
<input type="hidden" name="q_submitted" value="true">

  <table class="table table-condensed table_compute" id="table_compute" cellpadding="none" cellspacing="none">
  <tr><td colspan="4" style="background-color:#222222; color:white; padding:10px">PLEASE FILL-UP REQUIRED DETAILS</td></tr>

  <tr><td>
  Full Name
  </td><td>
  <input type="text" name="q_full_name" class="form-control" id="q_full_name" placeholder="Enter Full Name" required>
  </td></tr>
  <tr><td>
  Email
  </td><td>
  <input type="email" name="q_email" class="form-control" id="q_email" placeholder="Enter Best Email" required>
  </td></tr>
  <tr><td>
  Contact
  </td><td>
  <input type="text" name="q_contact" class="form-control" id="q_contact" placeholder="Enter Contact Number">
  </td></tr>

  <tr><td>
  Pick-up Location
  </td><td>
  <input type="text" name="q_pick_place" class="form-control" id="q_pick_place" placeholder="Airport/Hotel">
  </td></tr>
  <tr><td>
  Pick-up Date
  </td><td>
  <input class="form-control" id="q_pick_date" name="q_pick_date" placeholder="MM/DD/YYY" type="text"/>
  </td></tr>

  <tr><td><span class="impt_text">No. of Person(s)</td>
  <td>
    <select name="q_no_of_persons" class="form-control" id="q_no_of_persons" style="width:100%">
    </select>
  </td>
  </tr>
  <tr><td><span class="impt_text">No. of Day(s) Tour</td>
  <td>
    <select name="q_no_of_days" class="form-control" id="q_no_of_days" style="width:100%">
    </select>
  </td>
  </tr>
  <tr><td colspan=2>
  Additional Info<br/>
  <textarea class="noresize form-control" name="q_other_info" id="other_info"  rows="3" placeholder="Additional Info"></textarea>
  </td></tr>


  </table>

</div> <!-- col-xs-12 col-sm-4 -->

<style>

</style> 

<div class="col-xs-12 col-sm-7">

<div class="destinations">
<strong>CHECK DESIRED DESTINATION(S)</strong></div>
<label><input type="checkbox" name="tour3" value="Kawasan Falls and Canyoneering" checked/> Kawasan Falls and Canyoneering</label>
<label><input type="checkbox" name="tour2" value="Kawasan Falls" /> Kawasan Falls</label>
<label><input type="checkbox" name="tour1" value="Oslob Whale" /> Oslob Whale</label>
<label><input type="checkbox" name="tour4" value="Mactan Island Hopping"> Mactan Island Hopping</label>
<label><input type="checkbox" name="tour5" value="Tumalog Falls"> Tumalog Falls</label>
<label><input type="checkbox" name="tour6" value="Cebu City Tour"> Cebu City Tour</label>
<label><input type="checkbox" name="tour7" value="Simala"> Simala</label>
<label><input type="checkbox" name="tour8" value="Bohol Country Side Tour"> Bohol Country Side Tour</label>
<label><input type="checkbox" name="tour9" value="Pescador Island Hopping"> Pescador Island Hopping</label>
<label><input type="checkbox" name="tour10" value="10,000 Roses"> 10,000 Roses</label>
<label><input type="checkbox" name="tour11" value="Malapascua Island"> Malapascua Island</label>
<label><input type="checkbox" name="tour12" value="Kalanggaman Island"> Kalanggaman Island</label>

	<?php // echo do_shortcode( '[contact-form-7 id="77" title="Contact form 1"]' ); ?>
	<input  type="submit" value="BOOK MY TOUR NOW!" id="easy_book_submit">
</div>


</form> <!-- form_easybooking -->

</div> <!-- home contact -->

  <div class="kawasan_main clearfix row">

    <div class="col-sm-3" style="">
    <img src="<?php bloginfo('stylesheet_directory')?>/images/tours/customer.jpg" alt="Kawasan Canyoneering"/>
    </div>


    <div class="col-sm-6" style="">
    <h4 style="margin:0px;padding;0px"><span class="glyphicon glyphicon-ok-sign"></span> OUR CUSTOMERS</h4>
    <P>
Thank you very much for planning such a wonderful trip for us. We are very happy and satisfied with the services provided. Glad to have Jun with us throughout the trip. Personally we are quite touched by Jun's hospitality, patience and kindness. <br/>
<br/> 
Thank you once again for giving us an amazing lifetime experiences in Cebu and we'll definitely recommend to other friends if I'd a chance..<br/>
<br/> 
 
Regards, <br/>
Pei Gin and Friends <br/>
    </P>

    </div>

<div class="col-sm-3" id="TA_cdswritereviewlg991" class="TA_cdswritereviewlg">
<ul id="c4QiLzBKz9c" class="TA_links rfJNkE1q">
<li id="9YvXiwkt" class="dCE3Yzl">
<a target="_blank" href="https://www.tripadvisor.com.ph/"><img src="https://www.tripadvisor.com.ph/img/cdsi/img2/branding/medium-logo-12097-2.png" alt="TripAdvisor"/></a>
</li>
</ul>
</div>
<script async src="https://www.jscache.com/wejs?wtype=cdswritereviewlg&amp;uniq=991&amp;locationId=14788221&amp;lang=en_PH&amp;lang=en_PH&amp;display_version=2"></script>


  </div>


  <div class="kawasan_main clearfix row">
<?php
$args=array(
  'cat' => 69,
  'posts_per_page' => 1,
);
?>
  <?php
  $packagePosts = new WP_Query( $args );
  if( $packagePosts->have_posts() ) {
  //loop through related posts based on the tag
    while ( $packagePosts->have_posts() ) :
    $packagePosts->the_post();
  ?>

  <div class="col-sm-4">
  <a href="<?php the_permalink(); ?>">
    <?php the_post_thumbnail(); ?>
  </a>
  </div>  
  <div class="col-sm-8" style="">

   <h4 style="margin:0px;padding:0px"><span class="glyphicon glyphicon-ok-sign"></span>
   RECENT NEWS ...
   </h4>
   <div style="overflow:auto">
   <h4>
   <a href="<?php the_permalink(); ?>">  
   <?php the_title(); ?></a>
   </h4>
   <?php the_content(); ?>
   </div>  

  <?php endwhile; ?>
 <?php wp_reset_postdata(); ?>
  <?php } ?>

</div>



  


	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

<div class="row" style="background-color:#ffffff">

  <div class="col-sm-3 logo_others">
<a href="http://https://www.paypal.com/ph/home/">
<img src="<?php bloginfo('stylesheet_directory')?>/images/logo/paypal.jpg" alt="Kawasan Canyoneering"/>
</a>
  </div>
  <div class="col-sm-3 logo_others">
  <a href="http://www.tourism.gov.ph/">
<img src="<?php bloginfo('stylesheet_directory')?>/images/logo/itsmorefun.jpg" alt="Kawasan Canyoneering"/>
 </a>
  </div>
  <div class="col-sm-3 logo_others">
  <a href="http://cebuboholtraveltours.com/">
<img src="<?php bloginfo('stylesheet_directory')?>/images/logo/logocebubohol.jpg" alt="Kawasan Canyoneering"/>
</a>
  </div>
  <div class="col-sm-3 logo_others">
  <a href="https://whalesharkoslob.com/">
<img src="<?php bloginfo('stylesheet_directory')?>/images/logo/logo_label_whaleshark.jpg" alt="Kawasan Canyoneering"/>
</a>
  </div>

</div>




		<div class="site-info">
		<div>
		<a href="<?php bloginfo('stylesheet_directory')?>/downloads/cct_terms_and_conditions.pdf" target="_blank" class="btn btn-info" role="button">View Our Terms and Condition</a>
		</div>
			<a href="http://canyoneeringcebutours.com//"><?php echo '	&#9400; ' . date(" Y ") . ' Canyoneering Cebu Tours' ?></a>
			<br/>
      Member Company of <a href="http://cebuboholtraveltours.com/">CBTT Company</a>
      <br/>
			Powered by : <a href:'https://bawing.wordpress.com/'> EPP </a>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- row -->
</div><!-- container -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>


<script type="text/javascript">

jQuery(document).ready(function() {

    //initialize the number of persons select button
    for(i=1;i<=99;i++) {
      jQuery('#q_no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }

    for(i=1;i<=10;i++) {
      jQuery('#q_no_of_days').append('<option val="'+ i + '">' + i + '</option>');
    }
    submitQuick();

    // initialize the date picker
      var date_input=$('input[name="q_pick_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);

    // initialize the date picker
      var date_input=$('input[name="pick_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);


});


</script>
</html>


