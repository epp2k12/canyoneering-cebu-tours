<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package abm
 */

get_header(); ?>

<?php
	global $post;
	//echo '<pre>';
	//print_r($post);
	//echo '</pre>';
	//echo "<h1> $post->ID </h1>";
	$post_ID = $post->ID;
?>


<style type="text/css">
#gallery-1 img {
	border: 1px solid gray;
	padding: 3px;
}
</style>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();

		if(153 == $post_ID) {
			get_template_part('parts/template','oslobtours-pescadorcanyoneering');
			// get_template_part('parts/package','kitchen-remodeling');
		}elseif(156 == $post_ID) {
			get_template_part('parts/template','oslobtours-mactanisland');
		}elseif(159 == $post_ID) {
			get_template_part('parts/template','oslobtours-cebucity');
		}elseif(162 == $post_ID) {
			get_template_part('parts/template','oslobtours-malapascua');
		}elseif(173 == $post_ID) {
			get_template_part('parts/template','oslobtours-kalanggaman');
		}elseif(176 == $post_ID) {
			get_template_part('parts/template','oslobtours-oslobsumilonkawasan');
		}elseif(179 == $post_ID) {
			get_template_part('parts/template','oslobtours-oslobtumalogkawasan');
		}elseif(182 == $post_ID) {
			get_template_part('parts/template','oslobtours-kawasan');
		}elseif(185 == $post_ID) {
			get_template_part('parts/template','oslobtours-oslobtumalogsimala');
		}elseif(188 == $post_ID) {
			get_template_part('parts/template','oslobtours-oslobtumalogsumilon');
		}elseif(191 == $post_ID) {
			get_template_part('parts/template','oslobtours-oslobovernightbudget');
		}elseif(194 == $post_ID) {
			get_template_part('parts/template','oslobtours-oslobovernightfull');
		}elseif(197 == $post_ID) {
			get_template_part('parts/template','oslobtours-3d2ncebubohol');
		}elseif(200 == $post_ID) {
			get_template_part('parts/template','oslobtours-4d3ncebu');
		}elseif(203 == $post_ID) {
			get_template_part('parts/template','oslobtours-5d4ncebubohol');
		}elseif(206 == $post_ID) {
			get_template_part('parts/template','oslobtours-6d5ncebubohol');
		}elseif(213 == $post_ID) {
			get_template_part('parts/template','oslobtours-malapascuakalanggaman');

		}else {

			get_template_part( 'template-parts/content', get_post_format() );

			the_post_navigation();
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		}


		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();


