<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package abm
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head(); ?>



<!-- Isolated Version of Bootstrap, not needed if your site already uses Bootstrap -->
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />

<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>


</head>
<link rel="profile" href="http://gmpg.org/xfn/11">
<body <?php body_class(); ?>> 

<!-- <h1> THIS IS THE PORTIONS : <?php echo get_home_url(); ?> </h1> -->
<div id="page" class="site">
<div class="container">
<div class="row">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'abm' ); ?></a>
    
	<header id="masthead" class="site-header" role="banner" style="text-align:middle">

		<div class="site-branding">
		
		<div class="col-sm-2 header_items" id="site_logo">
		<a href="http://canyoneeringcebutours.com">
        <img style="margin:0;padding:0" src="<?php bloginfo('stylesheet_directory')?>/images/header/logo02.png" />
        </a>
		</div>

		<div class="col-sm-6 header_items" id="site_description">
        <h3 style="padding:3px;background-color:#222222;color:#ffffff">Canyoneering Cebu Tours</h3>
        <h6 style="padding:0px;margin:0px">Most Affordable Kawasan Canyoneering Tours and Car Rentals</h6>
        </div>

		<div class="col-sm-4 header_items" id="site_address">
		<span class="glyphicon glyphicon-home"></span> : Timpolok Rd., Lapu-lapu City Cebu<br/>		
		<span class="glyphicon glyphicon-earphone"></span> : +63 917 704 3508<br/>
		<span class="glyphicon glyphicon-earphone"></span> : +63 916 696 5614<br/>
		<span class="glyphicon glyphicon-envelope"></span>: info@canyoneeringcebutours.com<br/>
		<a href="https://www.facebook.com/abmmdularcabinetmanufacturing/" style="color:#ffffff"><img style="margin:0;padding:0" src="<?php bloginfo('stylesheet_directory')?>/images/social/fb3.png" /> Like us on Facebook </a><br/>
		
		</div>


		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'abm' ); ?></button> -->
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">

